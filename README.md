# annotation-service

## General information
The Annotation Gander Service is used to store comments that are made on either Pull Requests or Changes during review in the Gander platform. This allows comments to be kept locally, since we don't want to upload them to GitHub or Gerrit, while the service is running.
This means that any comment made will disappear if the service is restarted, allowing you to easily reset the server for a new experiment. 

## GitHub Authentication
Annotation Service needs a file called `credentials.txt` to be able to fetch comments and work properly with GitHub. If you don't have a personal access token in the header of your HTTP-request, after approx. 50 requests you will get an error code 403: Forbidden. 

This require you to:
* Have a GitHub account.
* [Follow this tutorial for creating a GitHub personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token). It's recommended having a separate one for working with Gander. Note that the token only requires read rights.
* Save the information in a file called `credentials.txt`. It should be located inside the `/app` folder. 
* The file should be in the format `username:AuthToken`, where username is your username on GitHub and AuthToken the token you just created. The username and the AuthToken should be separated by a `:`.
* This token is used in more services, so copy the file into them as well. Read the separate `README.md` files to see if the services need the authentication token. It is okay to use the same access token in multiple service. 

## API Endpoints

<details>
 <summary><code>get</code> <code><b>/comments/:mode/:number</b></code> <code>(Retrieves the comments stored in annotation service)</code></summary>

#### Parameters

> | Name   | Type   | Required | Description                     |
> |--------|--------|----------|---------------------------------|
> | number | String | Yes      | The pull request or change number identifier. |
> | mode   | String | Yes      | Gerrit mode or GitHub mode.         |

</details>

<details>
 <summary><code>post</code> <code><b>/comments/:number</b></code> <code>(Posts a comment to be stored in the annotation service)</code></summary>

#### Parameters

> | Name   | Type   | Required | Description                     |
> |--------|--------|----------|---------------------------------|
> | number | String | Yes      | The pull request or change number identifier. |

#### Request Body
> | Name        | Type    | Required | Description                                      |
> |------------|--------|----------|--------------------------------------------------|
> | body       | String | Yes      | The content of the comment.                     |
> | file       | String | Yes      | The file associated with the comment.           |
> | line       | String | Yes      | The line number in the file where the comment is placed. |
> | id         | String | Yes      | The unique identifier for the comment.          |
> | created_at | String | Yes      | The timestamp when the comment was created (`yyyy-MM-dd HH:mm:ss.SSS`). |
> | updated_at | String | Yes      | The timestamp when the comment was last updated (`yyyy-MM-dd HH:mm:ss.SSS`). |
> | user       | String | Yes      | The user who created the comment.               |
> | is_a_reply | Boolean | Yes     | Indicates if the comment is a reply (`true` or `false`). |
> | reply_to_id | String | No      | If the comment is a reply, this contains the ID of the parent comment. |

</details>