package annotation.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.logging.Logger;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

public class PingTest {
  Logger log = App.setupLogger(8000);
  private final PingService service = new PingService(log);

  /** Simple test scenario. */
  @Test
  public void appHasPing() {
    String pong = "pong";
    Request req1 = mock(Request.class);
    Response res1 = mock(Response.class);
    String serverResponse = service.ping(req1, res1);
    String comparison = "{\"message\":\"" + pong + "\"}";
    verify(res1).status(200);
    assertEquals(comparison, serverResponse);
  }
}
