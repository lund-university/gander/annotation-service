package annotation.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class for handling the storing and retrieval of comments. */
class CommentService {
  private static final String CLASS_NAME = CommentService.class.getName();
  private static final String GITHUB_COMMENT_ADDRESS = CommentService.getGithubServicePath();
  private static final String GERRIT_COMMENT_ADDRESS = CommentService.getGerritServicePath();
  private static final String COMMENT_PATH = "comments";
  private static final String REMOTE_COMMENTS = System.getenv("REMOTE_COMMENTS") != null
      ? System.getenv("REMOTE_COMMENTS")
      : "true";
  private Logger logger;
  private HashMap<String, ArrayList<Comment>> comments;

  /**
   * Constructor of this class.
   *
   * @param logger the Logger object used to debug log
   * 
   */
  public CommentService(Logger logger) {
    this.logger = logger;
    logger.info("Comment Service started");
    comments = new HashMap<String, ArrayList<Comment>>();
  }

  /**
   * 
   * @return the correct path depending on docker or not
   */
  public static String getGithubServicePath() {
    if (System.getenv("GITHUB_COMMENT_URL") != null) {
      return System.getenv("GITHUB_COMMENT_URL");
    }
    return "http://localhost:8001/pur/git/pulls/";
  }

  public static String getGerritServicePath() {
    if (System.getenv("GERRIT_COMMENT_URL") != null) {
      return System.getenv("GERRIT_COMMENT_URL");
    }
    return "http://localhost:8001/gerrit/changes/";
  }

  /**
   * Retrieves the comments stored on GitHub from the specific pull request
   * number.
   *
   * @param number the String representation of the Pull request number.
   * 
   * @return
   */
  public String getGitHubComments(String number) {
    String urlString = String.format("%s%s/%s", GITHUB_COMMENT_ADDRESS, number, COMMENT_PATH);
    logger.entering(CLASS_NAME, "getGitHubComments", urlString);
    try {
      HttpURLConnection c = HttpConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String result = JsonUtils.processResponse(c.getInputStream());

        logger.exiting(CLASS_NAME, "getGitHubComments", result);
        return result;
      }
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    return null;
  }

  /**
   * Retrieves the comments stored on Gerrit from the specific change number.
   *
   * @param number the String representation of the change number.
   * 
   * @return
   */
  private String getGerritComments(String number) {
    String urlString = String.format("%s%s/%s", GERRIT_COMMENT_ADDRESS, number, COMMENT_PATH);
    logger.entering(CLASS_NAME, "getGerritComments", urlString);
    try {
      HttpURLConnection c = HttpConnectionUtils.setConnectionNoHeaders(urlString, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String result = JsonUtils.processResponse(c.getInputStream());

        logger.exiting(CLASS_NAME, "getGerritComments", result);
        return result;
      }
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    return null;
  }

  /**
   * Retrieves the comments stored in the annotation service. It also retrieves
   * the comments stored in the GitHub or Gerrit repo/project.
   *
   * @param req the Request object
   * 
   * @param res the Response object
   * 
   * @return the found Comments as a JSON String
   */
  public String getComments(Request req, Response res) {
    logger.entering(CLASS_NAME, "getComments");
    String number = req.params(":number");
    String mode = req.params(":mode");

    String remoteComments = "[]";

    if (REMOTE_COMMENTS.equals("true")) {
      if (mode.equals("github")) {
        remoteComments = getGitHubComments(number);
      } else if (mode.equals("gerrit")) {
        remoteComments = getGerritComments(number);
      }
    }

    ArrayList<Comment> comArr = comments.get(number);
    if (comArr != null) {
      logger.exiting(CLASS_NAME, "getComments", comArr);
      return JsonUtils.formatFoundCommentResponse(comArr, remoteComments, number, logger);
    }

    if (remoteComments != null) {
      return JsonUtils.formatFoundOnlyRemoteComments(remoteComments, number);
    }

    String errorText = String.format("Pull request or change not found.");
    logger.exiting(CLASS_NAME, "getComments", "Pull request or change not found.");
    logger.severe(errorText);
    return JsonUtils.createErrorResponse(errorText);
  }

  /**
   * Puts the comment in the annotation service.
   *
   * @param req the Request object. Required params: Pull request / change number
   * 
   * @param res the Response object
   * 
   * @return an JSON String containing the OK response or an error message
   */
  public String putComment(Request req, Response res) {
    logger.entering(CLASS_NAME, "putComment");
    JSONObject obj = JsonUtils.parseJsonObject(req.body());
    if (obj == null) {
      String errorText = String.format("Parsing exception: %s", "Couldn't parse request");
      res.body(errorText);
      logger.exiting(CLASS_NAME, "putComment", errorText);
      logger.severe(errorText);
      return JsonUtils.createErrorResponse(errorText);
    }
    logger.fine(obj.toString());
    Comment newComment = new Comment(
        logger,
        obj.get("body").toString(),
        obj.get("file").toString(),
        obj.get("line").toString(),
        obj.get("id").toString(),
        obj.get("created_at").toString(),
        obj.get("updated_at").toString(),
        obj.get("user").toString());
    if (Boolean.valueOf(obj.get("is_a_reply").toString()) == true) {
      newComment.setReplyToId(obj.get("reply_to_id").toString());
    }

    String number = req.params(":number");
    logger.fine(newComment.toString());
    ArrayList<Comment> commentList = comments.get(number);
    if (commentList != null) {
      logger.fine(commentList.toString());
      comments.get(number).add(newComment);
      logger.exiting(CLASS_NAME, "putComment", comments.get(number));
      // TODO: Better return message, like JSON format
      return JsonUtils.createOkResponse(
          String.format("Comment for pull request or change was created at line %s", newComment.getLine()));
    } else {
      logger.fine("CommentList not found, creating a new one.");
      commentList = new ArrayList<Comment>();
      commentList.add(newComment);
      comments.put(number, commentList);
      logger.exiting(CLASS_NAME, "putComment", commentList);
      // TODO: Better return message, like JSON format
      return JsonUtils.createOkResponse(
          String.format("Comment for pull request or change was created at line %s", newComment.getLine()));
    }
  }
}
