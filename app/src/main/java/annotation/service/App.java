package annotation.service;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import spark.Filter;

public class App {
  private static final int DEFAULT_PORT = 8005;
  private static String logsFolderPath;

  /** Starts the service. */
  public static void main(String[] args) {
    // If no port is given by the arguments, it will go with the default port
    int port = args.length > 0 ? parsePortArgument(args[0]) : DEFAULT_PORT;
    port(port);
    Logger logger = setupLogger(port);
    logger.info("-- STARTING ANNOTATION SERVICE ON PORT " + port + " --");
    PingService s = new PingService(logger);
    CommentService comment = new CommentService(logger);

    after(
        (Filter)
            (request, response) -> {
              response.header("Access-Control-Allow-Origin", "*");
              response.header("Access-Control-Allow-Methods", "*");
            });
    // For testing the service
    get("/ping", (req, res) -> s.ping(req, res));
    // Retrieves the comments stored in annotation service
    get("/comments/:mode/:number", (req, res) -> comment.getComments(req, res));
    // Puts a comment in the annotation service
    post("/comments/:number", (req, res) -> comment.putComment(req, res));
  }

  /**
   * Parses the incoming argument that should be containing the port number.
   *
   * @param portNbr the port number as a String
   * @return the port number as an int
   */
  public static int parsePortArgument(String portNbr) {
    try {
      return Integer.parseInt(portNbr);
    } catch (NumberFormatException e) {
      System.out.println(
          "Could not resolve the argument to a port number, using the default port number "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }
  }

  /**
   * Initializes a Logger object. Creates a new entry in the log-folder.
   *
   * @param port port number the service is active on
   * @return the Logger object
   */
  public static Logger setupLogger(int port) {
    createLogFolder();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    Logger logger = Logger.getLogger(App.class.getName());
    FileHandler fh;
    try {
      fh =
          new FileHandler(
              logsFolderPath + "/Log_" + dtf.format(LocalDateTime.now()) + "_port" + port + ".log",
              true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh);
      // Change the line below for more/less logging info
      logger.setLevel(Level.ALL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return logger;
  }

  /** Creates a folder for the logs, if it doesn't already exist. */
  private static void createLogFolder() {
    logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));
    } catch (IOException e) {
      System.out.println("Error while creating the logs directory. ");
    }
  }
}
