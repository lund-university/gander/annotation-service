package annotation.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

/** Class for representing the stored comments. */
public class Comment {
  private int id;
  private String content;
  private String line;
  private String file;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private String replyToId;
  private String user;

  /**
   * Constructor.
   *
   * @param logger logger
   * @param content content
   * @param file file
   * @param line line
   * @param id id
   * @param createdAt createdAt
   * @param updatedAt updatedAt
   * @param user user
   * 
   */
  public Comment(
      Logger logger,
      String content,
      String file,
      String line,
      String id,
      String createdAt,
      String updatedAt,
      String user) {
    this.file = file;
    this.line = line;
    this.content = content;
    this.id = Integer.valueOf(id);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    this.createdAt = LocalDateTime.parse(createdAt, formatter);
    this.updatedAt = LocalDateTime.parse(updatedAt, formatter);
    this.user = user;
    this.replyToId = "-1";
  }

  /**
   * Returns id. 
   *
   *  @return the comment ID 
   * 
   */
  public int getId() {
    return id;
  }

  /** 
   * returns content.
   *
   * @return the comment content 
   * 
   */
  public String getContent() {
    return content;
  }

  /** 
   * getter for line.
   *
   * @return the line where the comment was made 
   * 
   */
  public String getLine() {
    return line;
  }

  /**
   * getter of file.
   *
   * @return the String representation of the file name 
   * 
   */
  public String getFile() {
    return file;
  }

  /**
   * getter of user.
   *
   * @return the name of the user that posted the comment 
   * 
   */
  public String getUser() {
    return user;
  }

  /**
   * getter for creaedAt.
   *
   * @return the String format of the LocalDateTime when the comment was created 
   * 
   */
  public String getCreatedAt() {
    return createdAt.toString();
  }

  /**
   * getter for updatedAt.
   *
   * @return the String format of the LocalDateTime when the comment was updated 
   * 
   */
  public String getUpdatedAt() {
    return updatedAt.toString();
  }

  /**
   * setter for ReplyToId.
   *
   * @param replyToId the String representation of the comment ID that the comment is a response to
   *
   */
  public void setReplyToId(String replyToId) {
    this.replyToId = replyToId;
  }

  /**
   * getter for replyToId.
   *
   * @return the String representation of the comment ID that it is a response to 
   * 
   */
  public String getReplyToId() {
    return replyToId;
  }
}
