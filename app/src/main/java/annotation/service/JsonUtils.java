package annotation.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** A utils class for handling JSON Objects. */
public class JsonUtils {

  /**
   * Parses the response as a JSON Object.
   *
   * @param response Response from a request
   * @return JSON Object
   */
  public static JSONObject parseJsonObject(String response) {
    JSONParser parser = new JSONParser();
    JSONObject json;
    try {
      json = (JSONObject) parser.parse(response);

    } catch (Exception e) {
      return null;
    }
    return json;
  }

  /**
   * Parses the response as a JSON Array.
   *
   * @param response Response String from a request
   * @return JSON Array
   */
  public static JSONArray parseJsonArray(String response) {
    JSONParser parser = new JSONParser();
    JSONArray json;
    try {
      json = (JSONArray) parser.parse(response);

    } catch (Exception e) {
      return null;
    }
    return json;
  }

  /**
   * Parses the response from a request as a String.
   *
   * @param res response from a request
   * @return String containing the response
   */
  public static String processResponse(InputStream res) {
    try {
      BufferedReader in = new BufferedReader(new InputStreamReader(res));
      String inputLine;
      StringBuffer resp = new StringBuffer();

      while ((inputLine = in.readLine()) != null) {
        resp.append(inputLine);
      }
      in.close();
      return resp.toString();
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * A method for creating OK JSON Strings.
   *
   * @param content The message content
   * @return a JSON String with status OK and the specified message
   */
  public static String createOkResponse(String content) {
    return String.format("{ \"status\" : \"OK\", \"message\" : \"%s\"}", content);
  }

  /**
   * A method for creating error JSON Strings.
   *
   * @param content The message content
   * @return a JSON String with status ERROR and the specified message
   */
  public static String createErrorResponse(String content) {
    return String.format("{ \"status\" : \"ERROR\", \"message\" : \"%s\"}", content);
  }

  /**
   * Gathers all of the comments from 'remoteComments' and from the local storage 'comments' and
   * produces a JSON String containing their information.
   *
   * @param comments The comments stored in the annotation service
   * @param remoteComments The comments stored in GitHub or Gerrit
   * @param number Pull request number
   * @param logger The logger object
   * @return A String containing the found comments
   */
  public static String formatFoundCommentResponse(
      ArrayList<Comment> comments, String remoteComments, String number, Logger logger) {
    JSONArray res = JsonUtils.parseJsonArray(remoteComments);

    for (Comment comment : comments) {
      JSONObject commentObj = new JSONObject();
      commentObj.put("file", comment.getFile());
      commentObj.put("body", comment.getContent());
      commentObj.put("id", comment.getId());
      commentObj.put("line", comment.getLine());
      commentObj.put("user", comment.getUser());
      commentObj.put("createdAt", comment.getCreatedAt());
      commentObj.put("updatedAt", comment.getUpdatedAt());
      commentObj.put("reply_to_id", comment.getReplyToId());
      if (!comment.getReplyToId().equals("-1")) {
        commentObj.put("is_a_reply", true);
      }

      res.add(commentObj);
    }

    JSONObject returnObj = new JSONObject();
    returnObj.put("number", number);
    returnObj.put("comments", res);
    logger.exiting("JSONUtils", "formatFoundCommentResponse", returnObj.toString());
    return returnObj.toString();
  }

  /**
   * Method for retrieving only the comments stored on GitHub or Gerrit, and not in the annotation service.
   * Often used when there are no comments currently stored in the annotation service.
   *
   * @param remoteComments The comments stored on GitHub or Gerrit
   * @param number Pull request number
   * @return A String containing the found comments
   */
  public static String formatFoundOnlyRemoteComments(String remoteComments, String number) {
    JSONArray comments = JsonUtils.parseJsonArray(remoteComments);
    JSONObject returnObj = new JSONObject();
    returnObj.put("number", number);
    returnObj.put("comments", comments);
    return returnObj.toString();
  }
}
